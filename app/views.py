from flask import render_template, url_for, redirect, g, request ##
from app import app
from forms import TaskForm
import json ##

# rethink imports
import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError, RqlDriverError

# rethink config
RDB_HOST =  'localhost'
RDB_PORT = 28015
TASKQ_DB = 'task_queue'

# db setup; only run once
def dbSetup():
    connection = r.connect(host=RDB_HOST, port=RDB_PORT)
    try:
        r.db_create(TASKQ_DB).run(connection)
        r.db(TASKQ_DB).table_create('TASKQs').run(connection)
        print 'The Database setup has been completed'
    except RqlRuntimeError:
        print 'The Database setup already exists.'
    finally:
        connection.close()
dbSetup()

# open connection before each request
@app.before_request
def before_request():
    try:
        g.rdb_conn = r.connect(host=RDB_HOST, port=RDB_PORT, db=TASKQ_DB)
    except RqlDriverError:
        abort(503, "Database connection couldn't be established.")

# close the connection after each request
@app.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass

@app.route('/', methods = ['GET', 'POST'])
def index():
        form = TaskForm()
        if form.validate_on_submit(): 
                r.table('TASKQs').insert({"name":form.label.data}).run(g.rdb_conn)
                return redirect(url_for('index'))
        selection = list(r.table('TASKQs').run(g.rdb_conn))
        return render_template('index.html', form = form, tasks = selection)
